<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::post('Register', 'Auth\AuthController@create');
Route::get('/login','AuthController@login');
Route::post('test','authController@postUser');
// Route::group(array('prefix' => 'api'), function() { 
// 	Route::post('login', 'Auth\AuthController@login'); 
	
// 	 });
// Route::get('/', function () {
//     return view('welcome');
// });
    // Route::get('request',function(){
    // return 'this is my first response';
    // }); 

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
